'use strict'

const Router = require('express').Router
const passport = require('passport')
const GitlabStrategy = require('passport-gitlab2').Strategy
const config = require('../../../config')
const response = require('../../../response')
const { setReturnToFromReferer, passportGeneralCallback } = require('../utils')
const request = require('request');

function groupCallback(accessToken, refreshToken, profile, done) {
  const GROUP_URL = 'https://gitlab.com/api/v4/groups/5277179/members/all?private_token=' + config.gitlab.groupToken;
  request.get({url: GROUP_URL, json:true}, function(e, r, body) {
    let valid = false;
    for (let member of body) {
      if (profile.id == member.id.toString()) {
        valid = true;
        break;
      }
    }
    if (valid) {
      passportGeneralCallback(accessToken, refreshToken, profile, done);
    } else {
      done(null, false, {message: 'Not a gitlab group member'});
    }
  });
}

let gitlabAuth = module.exports = Router()

passport.use(new GitlabStrategy({
  baseURL: config.gitlab.baseURL,
  clientID: config.gitlab.clientID,
  clientSecret: config.gitlab.clientSecret,
  scope: config.gitlab.scope,
  callbackURL: config.serverURL + '/auth/gitlab/callback'
}, groupCallback))

gitlabAuth.get('/auth/gitlab', function (req, res, next) {
  setReturnToFromReferer(req)
  passport.authenticate('gitlab')(req, res, next)
})

// gitlab auth callback
gitlabAuth.get('/auth/gitlab/callback',
  passport.authenticate('gitlab', {
    successReturnToOrRedirect: config.serverURL + '/',
    failureRedirect: config.serverURL + '/'
  })
)

if (!config.gitlab.scope || config.gitlab.scope === 'api') {
  // gitlab callback actions
  gitlabAuth.get('/auth/gitlab/callback/:noteId/:action', response.gitlabActions)
}
